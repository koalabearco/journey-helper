import React from 'react';

const Footer = () => {
    return(
        <footer className="footer has-background-dark">
            <div className="content has-text-centered">
                <p>
                    Powered by <a href="https://reactjs.org" rel="noreferrer nofollow noopener" target="_blank">React</a>. Built by <a href="https://www.koalabear.co/" rel="noreferrer nofollow noopener" target="_blank">Koala Bear Co.</a>
                </p>
            </div>
        </footer>
    );
};

export default Footer;
