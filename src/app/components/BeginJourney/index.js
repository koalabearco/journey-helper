import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Footer from "../Footer";

class BeginJourney extends Component {
    constructor(props) {
        super(props);
        this.state = {numberArraySize: 5}
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        return this.setState({numberArraySize: event.target.value});
    }

    render() {
        const {numberArraySize} = this.state;
        return (
            <div className='container has-background-black'>
                <section className="hero is-dark">
                    <div className="hero-body">
                        <div className="container has-text-centered">
                            <h1 className="title">
                                <Link to='/' className="link">Begin Your Journey...</Link>
                            </h1>
                        </div>
                    </div>
                </section>
                <section className='section'>
                    <section className='section'>
                        <Link to='/two-variable-journey'
                              className='button is-medium is-fullwidth is-primary is-outlined'>
                            <strong>A B</strong>
                        </Link>
                    </section>
                    <section className='section'>
                        <Link to='/three-variable-journey'
                              className='button is-medium is-fullwidth is-primary is-outlined'>
                            <strong>A B C</strong>
                        </Link>
                    </section>
                    <section className='section'>
                        <Link to='/four-variable-journey'
                              className='button is-medium is-fullwidth is-primary is-outlined'>
                            <strong>A B C D</strong>
                        </Link>
                    </section>
                    <section className='section'>
                        <div className="field has-addons">
                            <div className="control is-expanded">
                                <input
                                    className="input is-large"
                                    type="number"
                                    min="2"
                                    max="30"
                                    step="1"
                                    name="numberArraySize"
                                    onChange={this.handleChange}
                                    value={numberArraySize}
                                />
                            </div>
                            <div className="control">
                                <Link
                                    to={`/number-shuffle-journey/${(this.state.numberArraySize > 2) ? this.state.numberArraySize : 2}`}
                                    className="button is-primary is-large is-outlined">
                                    Number Shuffle
                                </Link>
                            </div>
                        </div>
                    </section>
                </section>
                <Footer/>
            </div>
        );
    }
}

export default BeginJourney;
