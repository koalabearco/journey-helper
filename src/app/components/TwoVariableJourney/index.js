import React, {Component} from 'react';
import shuffle from 'shuffle-array';
import {Link} from "react-router-dom";
import Loader from "../Loader";
import Footer from "../Footer";

class TwoVariableJourney extends Component {
    constructor(props) {
        super(props);
        this.state = {A: "", B: "", isVisible: false};
        this.viewAnswer = this.viewAnswer.bind(this);
        this.resetAnswer = this.resetAnswer.bind(this);
    }

    componentDidMount() {
        let collection = ["Yes", "No"];
        shuffle(collection);
        this.setState({A: collection[0], B: collection[1], isVisible: false});
    }

    viewAnswer() {
        this.setState({isVisible: true});
    }

    resetAnswer() {
        let collection = ["Yes", "No"];
        shuffle(collection);
        this.setState({A: collection[0], B: collection[1], isVisible: false});
    }

    render() {
        const {isVisible, A, B} = this.state;
        return (
            <div className="container has-background-black">
                <section className="hero is-dark">
                    <div className="hero-body">
                        <div className="container has-text-centered">
                            <h1 className="title">
                                <Link to='/' className="link">Begin Your Journey...</Link>
                            </h1>
                        </div>
                    </div>
                </section>
                <section className="hero is-black is-small">
                    <div className="hero-body has-text-centered has-text-white">
                        A&nbsp;B
                    </div>
                </section>
                {isVisible
                    ? <section className="section">
                        <nav className="level is-mobile">
                            <div className="level-item has-text-centered">
                                <div>
                                    <p className="heading">A</p>
                                    <p className="title has-text-primary">{A}</p>
                                </div>
                            </div>
                            <div className="level-item has-text-centered">
                                <div>
                                    <p className="heading">B</p>
                                    <p className="title has-text-primary">{B}</p>
                                </div>
                            </div>
                        </nav>
                    </section>
                    : <Loader/>
                }
                {isVisible
                    ? <section className='section'>
                        <button
                            className='button is-medium is-fullwidth is-primary is-outlined'
                            onClick={() => this.resetAnswer()}
                        >
                            Reset
                        </button>
                    </section>
                    : <section className='section'>
                        <button
                            className='button is-medium is-fullwidth is-primary is-outlined'
                            disabled={isVisible}
                            onClick={() => this.viewAnswer()}
                        >
                            View
                        </button>
                    </section>
                }

                <Footer/>
            </div>
        );
    }

}

export default TwoVariableJourney;
