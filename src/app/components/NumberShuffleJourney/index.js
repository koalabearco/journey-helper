import React, {Component} from 'react';
import {Link} from "react-router-dom";
import shuffle from "shuffle-array";
import Loader from "../Loader";
import range from './range';
import Footer from "../Footer";

class NumberShuffleJourney extends Component {
    constructor(props) {
        super(props);
        this.state = {numberArray: [], isVisible: false};
        this.viewAnswer = this.viewAnswer.bind(this);
        this.resetAnswer = this.resetAnswer.bind(this);
    }

    componentDidMount() {
        let arraySize = this.props.match.params.arraySize;
        let collection = range(1, arraySize);
        shuffle(collection);
        this.setState({numberArray: collection, isVisible: false})
    }

    viewAnswer() {
        this.setState({isVisible: true});
    }

    resetAnswer() {
        let arraySize = this.props.match.params.arraySize;
        let collection = range(1, arraySize);
        shuffle(collection);
        this.setState({numberArray: collection, isVisible: false})
    }

    render() {
        const {numberArray, isVisible} = this.state;
        return (
            <div className="container has-background-black">
                <section className="hero is-dark">
                    <div className="hero-body">
                        <div className="container has-text-centered">
                            <h1 className="title">
                                <Link to='/' className="link">Begin Your Journey...</Link>
                            </h1>
                        </div>
                    </div>
                </section>
                <section className="hero is-black is-small">
                    <div className="hero-body has-text-centered has-text-white">
                        Number Shuffle
                    </div>
                </section>
                {isVisible
                    ? numberArray.map(number => {
                        return (
                            <section key={number} className="hero is-dark">
                                <div className="hero-body">
                                    <div className="has-text-centered">
                                        <h1 className="title has-text-primary">
                                            {number}
                                        </h1>
                                    </div>
                                </div>
                            </section>
                        )
                    })
                    : <Loader/>
                }

                {isVisible
                    ? <section className='section'>
                        <button
                            className='button is-medium is-fullwidth is-primary is-outlined'
                            onClick={() => this.resetAnswer()}
                        >
                            Reset
                        </button>
                    </section>
                    : <section className='section'>
                        <button
                            className='button is-medium is-fullwidth is-primary is-outlined'
                            disabled={isVisible}
                            onClick={() => this.viewAnswer()}
                        >
                            View
                        </button>
                    </section>
                }
                <Footer/>
            </div>
        );
    }
}

export default NumberShuffleJourney;
