import React from 'react';
import grid from '../../assets/images/grid.svg';

const Loader = () => (<section className="hero is-info">
    <div className="hero-body has-text-centered">
        <img src={grid} alt="grid-loader" height="51" width="51"/>
    </div>
</section>);

export default Loader;
