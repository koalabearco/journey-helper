import React from 'react';
import {Route, Switch} from "react-router-dom";
import "./assets/sass/styles.sass"
import BeginJourney from './components/BeginJourney';
import TwoVariableJourney from "./components/TwoVariableJourney";
import ThreeVariableJourney from "./components/ThreeVariableJourney";
import FourVariableJourney from "./components/FourVariableJourney";
import NumberShuffleJourney from "./components/NumberShuffleJourney";

const App = () => (
    <div>
        <Switch>
            <Route exact path="/" component={BeginJourney}/>
            <Route exact path="/two-variable-journey/" component={TwoVariableJourney}/>
            <Route exact path="/three-variable-journey/" component={ThreeVariableJourney}/>
            <Route exact path="/four-variable-journey/" component={FourVariableJourney}/>
            <Route path="/number-shuffle-journey/:arraySize/" component={NumberShuffleJourney}/>
        </Switch>
    </div>
);

export default App;